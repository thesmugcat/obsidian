import Item5e from '../../../../systems/dnd5e/module/item/entity.js';
import {Prepare} from '../rules/prepare.js';
import {Schema} from './schema.js';
import {spellNotes} from '../rules/spells.js';
import {OBSIDIAN} from '../global.js';
import {Effect} from './effect.js';

export function patchItem_prepareData () {
	Item5e.prototype.prepareData = (function () {
		const cached = Item5e.prototype.prepareData;
		return function (attackList, effectMap, componentMap, toggleList) {
			cached.apply(this, arguments);
			prepareEffects(this.actor, this.data, attackList, effectMap, componentMap, toggleList);
		};
	})();
}

export function getSourceClass (actorData, source) {
	if (source.type === 'class') {
		return actorData.obsidian.classes.find(cls => cls._id === source.class);
	} if (source.type === 'item') {
		const parent = actorData.obsidian.itemsByID.get(source.item);
		if (parent
			&& getProperty(parent, 'flags.obsidian.source')
			&& parent.flags.obsidian.source.type === 'class')
		{
			return actorData.obsidian.classes.find(cls =>
				cls._id === parent.flags.obsidian.source.class);
		}
	}
}

const prepareComponents = {
	attack: function (actorData, item, effect, component, cls) {
		Prepare.calculateHit(actorData, item, component, cls);
		Prepare.calculateAttackType(item.flags.obsidian, component);
	},

	damage: function (actorData, item, effect, component, cls) {
		Prepare.calculateDamage(actorData, item, component, cls);
	},

	save: function (actorData, item, effect, component, cls) {
		Prepare.calculateSave(actorData, item, component, cls);
	},

	resource: function (actorData, item, effect, component) {
		Prepare.calculateResources(actorData, item, effect, component);

		component.label =
			component.name.length ? component.name : game.i18n.localize('OBSIDIAN.Unnamed');

		item.flags.obsidian.notes.push(
			'<div class="obsidian-table-note-flex">'
				+ `<div data-roll="fx" data-uuid="${effect.uuid}" class="rollable">`
					+ component.label
				+ `</div>: ${component.display}`
			+ '</div>');
	},

	target: function (actorData, item, effect, component) {
		if (component.target === 'area' && !effect.isLinked) {
			item.flags.obsidian.notes.push(
				`${component.distance} ${game.i18n.localize('OBSIDIAN.FeetAbbr')} `
				+ game.i18n.localize(`OBSIDIAN.Target-${component.area}`));
		}
	},

	consume: function (actorData, item, effect, component) {
		if (component.calc === 'var') {
			component.fixed = 1;
		}

		if (component.target === 'this-item' || component.target === 'this-effect') {
			component.itemID = item._id;
		}
	},

	spells: function (actorData, item, effect, component) {
		if (component.source === 'individual' && component.method === 'list') {
			const cls = actorData.obsidian.classes.find(cls => cls._id === component.class);
			component.spells.forEach(id => {
				const spell = actorData.obsidian.itemsByID.get(id);
				if (!spell) {
					return;
				}

				spell.flags.obsidian.visible = false;
				if (cls && getProperty(cls, 'flags.obsidian.spellcasting.spellList')) {
					cls.flags.obsidian.spellcasting.spellList.push(spell);
				}
			});
		} else if (component.source === 'list'
			&& getProperty(item, 'flags.obsidian.source.type') === 'class'
			&& OBSIDIAN.Data.SPELLS_BY_CLASS[component.list])
		{
			const cls = actorData.obsidian.classes.find(cls =>
				cls._id === item.flags.obsidian.source.class);

			if (!cls || !getProperty(cls, 'flags.obsidian.spellcasting.spellList')) {
				return;
			}

			const list = cls.flags.obsidian.spellcasting.spellList;
			const existing = new Set(list.map(spell => spell._id));

			cls.flags.obsidian.spellcasting.spellList =
				list.concat(
					OBSIDIAN.Data.SPELLS_BY_CLASS[component.list]
						.filter(spell => !existing.has(spell._id)));
		}

		if (component.source === 'individual' && component.method === 'item') {
			item.flags.obsidian.notes.push(...component.spells
				.map(id => actorData.obsidian.itemsByID.get(id))
				.filter(_ => _)
				.map(spell =>
					'<div class="obsidian-table-note-flex">'
					+ `<div data-roll="item" data-id="${spell._id}" class="rollable">`
					+ `${spell.name}</div></div>`));
		}
	}
};

prepareComponents.produce = prepareComponents.consume;

export function prepareEffects (actor, item, attackList, effectMap, componentMap) {
	if (!item.flags || !item.flags.obsidian || !actor) {
		return;
	}

	const actorData = actor.data;
	if (!actorData.flags
		|| !actorData.obsidian
		|| !actorData.flags.obsidian
		|| (actorData.flags.obsidian.version || 0) < Schema.VERSION)
	{
		return;
	}

	const data = actorData.data;
	const flags = item.flags.obsidian;
	const effects = flags.effects || [];
	flags.notes = [];

	if (item.type === 'equipment' && item.flags.obsidian.armour) {
		Prepare.armourNotes(item);
	}

	if (item.type === 'weapon') {
		Prepare.weaponNotes(item);
	}

	item.obsidian = {
		actionable: [],
		collection: {versatile: []}
	};

	Effect.metadata.components.forEach(c => item.obsidian.collection[c] = []);

	let cls;
	if (flags.source) {
		cls = getSourceClass(actorData, flags.source);
	}

	for (let effectIdx = 0; effectIdx < effects.length; effectIdx++) {
		const effect = effects[effectIdx];
		if (effectMap) {
			effectMap.set(effect.uuid, effect);
		}

		effect.parentActor = actorData._id;
		effect.parentItem = item._id;
		effect.idx = effectIdx;
		effect.label = getEffectLabel(effect);
		effect.applies = [];
		effect.isLinked = false;

		Effect.metadata.single.forEach(single => effect[`${single}Component`] = null);
		Effect.metadata.linked.forEach(linked => {
			const found = effect.components.find(c => c.type === linked);
			const bool = `is${linked.capitalise()}`;
			const self = `self${linked.capitalise()}`;
			const component = `${linked}Component`;
			effect[bool] = !!found;
			effect[self] = found && found.ref === effect.uuid;
			effect[component] = found;
			effect.isLinked |= effect[bool] && !effect[self];

			if (found) {
				item.obsidian.collection[linked].push(effect);
			}
		});

		for (let componentIdx = 0; componentIdx < effect.components.length; componentIdx++) {
			const component = effect.components[componentIdx];
			if (componentMap) {
				componentMap.set(component.uuid, component);
			}

			component.parentEffect = effect.uuid;
			component.idx = componentIdx;

			if (Effect.metadata.single.has(component.type)) {
				effect[`${component.type}Component`] = component;
			} else if (!effect.isLinked) {
				let collection = component.type;
				if (component.type === 'damage' && component.versatile) {
					collection = 'versatile';
				}

				item.obsidian.collection[collection].push(component);
			}

			const prepare = prepareComponents[component.type];
			if (prepare) {
				prepare(actorData, item, effect, component, cls);
			}
		}

		if (effect.targetComponent && effect.targetComponent.target === 'individual') {
			effect.components
				.filter(c => c.type === 'attack')
				.forEach(atk => atk.targets = effect.targetComponent.count);
		}

		if (!effect.isLinked && !effect.components.some(c => Effect.metadata.active.has(c.type))) {
			item.obsidian.actionable.push(effect);
		}

		const isRollable =
			effect.selfApplied || effect.components.some(c => Effect.metadata.rollable.has(c.type));

		if (isRollable
			&& item.type !== 'spell'
			&& !effect.components.some(c =>
				c.type === 'resource'
				|| c.type === 'attack'
				|| (c.type === 'spells' && c.source === 'individual' && c.method === 'item')))
		{
			flags.notes.push(
				'<div class="obsidian-table-note-flex">'
					+ `<div data-roll="fx" data-uuid="${effect.uuid}" class="rollable">`
						+ effect.label
					+ '</div>'
				+ '</div>');
		}
	}

	item.obsidian.collection.applied.forEach(e =>
		effectMap?.get(e.appliedComponent.ref)?.applies.push(e.uuid));

	item.obsidian.actionable = item.obsidian.actionable.flatMap(action => {
		const spells = action.components.filter(c => c.type === 'spells');
		if (spells.length) {
			return spells.flatMap(spell =>
				spell.spells.map(id => actorData.obsidian.itemsByID.get(id)));
		} else {
			return action;
		}
	});

	if (item.type === 'spell') {
		spellNotes(item);
	}

	if (item.type === 'spell' && item.data.level === 0) {
		const cantripScaling =
			item.obsidian.collection.scaling.find(effect =>
				effect.scalingComponent.method === 'cantrip');

		if (cantripScaling) {
			// Cantrips are scaled up-front, not when rolled.
			const extra = Math.round((data.details.level + 1) / 6 + .5) - 1;
			if (extra > 0) {
				const targetComponent =
					cantripScaling.components.find(c => c.type === 'target');
				const damageComponents =
					cantripScaling.components.filter(c => c.type === 'damage');

				if (targetComponent) {
					item.obsidian.collection.attack.forEach(atk =>
						atk.targets += targetComponent.count * extra);
				} else if (damageComponents.length) {
					damageComponents.forEach(dmg => dmg.scaledDice = extra);
					item.obsidian.collection.damage =
						item.obsidian.collection.damage.concat(damageComponents);
				}
			}
		}
	}

	if (item.obsidian.collection.attack.length) {
		if (attackList && (item.type !== 'weapon' || item.data.equipped)) {
			attackList.push(...item.obsidian.collection.attack);
		}

		item.obsidian.bestAttack =
			item.obsidian.collection.attack.reduce((acc, atk) => atk.value > acc.value ? atk : acc);

		if (item.obsidian.bestAttack.targets > 1) {
			flags.notes.push(
				`${game.i18n.localize('OBSIDIAN.Count')}: `
				+ item.obsidian.bestAttack.targets);
		}
	} else if (item.obsidian.collection.damage.length) {
		const targetComponents =
			effects.filter(effect => !effect.isLinked)
				.flatMap(effect => effect.components)
				.filter(c => c.type === 'target' && c.target === 'individual');

		if (targetComponents.length) {
			flags.notes.push(
				`${game.i18n.localize('OBSIDIAN.Count')}: ${targetComponents[0].count}`);
		}
	}

	if (item.obsidian.collection.save.length) {
		item.obsidian.bestSave =
			item.obsidian.collection.save.reduce((acc, save) =>
				save.value > acc.value ? save : acc);
	}

	if (item.obsidian.collection.resource.length) {
		item.obsidian.bestResource =
			item.obsidian.collection.resource.reduce((acc, resource) =>
				resource.max > acc.max ? resource: acc);
	}
}

export function getEffectLabel (effect) {
	if (effect.name.length) {
		return effect.name;
	}

	return game.i18n.localize('OBSIDIAN.Unnamed');
}
